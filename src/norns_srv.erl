%%%-------------------------------------------------------------------
%%% @author Chaitanya Chalasani
%%% @copyright (C) 2021, ArkNode.IO
%%% @doc
%%%
%%% @end
%%% Created : 2021-06-22 04:38:26.574758
%%%-------------------------------------------------------------------
-module(norns_srv).

-behaviour(gen_server).

-define(TIMER, 5000).

%% API
-export([start_link/0]).

%% gen_server callbacks
-export([init/1,
         handle_call/3,
         handle_cast/2,
         handle_info/2,
         terminate/2,
         code_change/3]).

%% spawn export
-export([run/1]).

%%%===================================================================
%%% API
%%%===================================================================

start_link() ->
  gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

init([]) ->
  erlang:send_after(?TIMER, self(), tick),
  {ok, #{}}.

handle_call(_Request, _From, State) ->
  Reply = ok,
  {reply, Reply, State}.

handle_cast(_Msg, State) ->
  {noreply, State}.

handle_info(tick, State) ->
  erlang:send_after(?TIMER, self(), tick),
  run(),
  {noreply, State};
handle_info(_Info, State) ->
  {noreply, State}.

terminate(_Reason, _State) ->
  ok.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

run() ->
  {{YYYY, MM, DD}, {H, M, _}} = NowDateTime = erlang:universaltime(),
  lists:foreach(
    fun(#{name := Name, run_time := ThenDateTime, timezone := Timezones} = Job) ->
        case ThenDateTime of
          {{YYYY, MM, DD}, {H, M, _}} ->
            ok;
          _ ->
            NowInTimezones = now_in_timezones(NowDateTime, Timezones),
            case is_schedule_now(Job, NowInTimezones) of
              true ->
                R = norns:update_jobs(#{name => Name}, #{run_time => NowDateTime}),
                lager:info("~p's schedule is now so updated ~p run_time ~p and spawning the runner"
                           ,[Name, R, NowDateTime]),
                proc_lib:spawn(?MODULE, run, [Job#{run_time => NowDateTime}]);
              false -> ok
            end
        end
    end,
    norns:get_jobs(#{status => <<"active">>})
   ).

now_in_timezones(NowDateTime, undefined) -> [NowDateTime];
now_in_timezones(NowDateTime, Timezones) ->
  lists:foldl(
    fun(Timezone, NowInTimezones) ->
        case qdate:to_date(Timezone, NowDateTime) of
          {error, _Reason} -> NowInTimezones;
          NowInTimezone -> [NowInTimezone|NowInTimezones]
        end
    end,
    [NowDateTime],
    Timezones
   ).

is_schedule_now(#{minutes := Ms, hours := Hs, days_month := DMs, days_week := DWs, month := MMs}
                ,NowInTimezones) ->
  lists:any(
    fun({{YYYY, MM, DD}, {H, M, _}}) ->
        DW = calendar:day_of_the_week(YYYY, MM, DD),
        lists:all(
          fun({[], _}) -> true;
             ({undefined, _}) -> true;
             ({Xs, X}) when is_list(Xs) -> lists:member(X, Xs)
          end,
          [{Ms, M}, {Hs, H}, {DMs, DD}, {DWs, DW}, {MMs, M}]
         )
    end,
    NowInTimezones
   ).

run(#{name := Name, function := Function, run_time := RunTime}) ->
  lager:info("Starting ~p on ~p", [Name, Function]),
  Result = apply_function(Function),
  Pid = self(),
  R = norns:put_log(#{name => Name, run_time => RunTime
                           ,run_pid => Pid, run_result => Result}),
  lager:info("Completed ~p with ~p and updated log ~p", [Name, Result, R]).

apply_function({M, F, A}) -> (catch apply(M, F, A));
apply_function({Fun, A}) -> (catch apply(Fun, A));
apply_function(Fun) when is_function(Fun, 0) -> (catch Fun());
apply_function(Fun) ->
  lager:error("Bad function ~p", [Fun]),
  failed.
