-module(norns_tivan).

-behaviour(tivan_server).

-export([start_link/0
        ,put_job/1
        ,get_jobs/0
        ,get_jobs/1
        ,update_jobs/2
        ,delete_job/1
        ,put_log/1
        ,get_logs/0
        ,get_logs/1
        ,update_logs/2
        ,delete_log/1]).

-export([init/1]).

%%%===================================================================
%%% API
%%%===================================================================

start_link() ->
  tivan_server:start_link({local, ?MODULE}, ?MODULE, [], []).

put_job(Job) when is_map(Job) ->
  tivan_server:put(?MODULE, norn_jobs, Job).

get_jobs() ->
  get_jobs(#{}).

get_jobs(Options) when is_map(Options) ->
  tivan_server:get(?MODULE, norn_jobs, Options).

update_jobs(Options, Updates) when is_map(Options), is_map(Updates) ->
  tivan_server:update(?MODULE, norn_jobs, Options, Updates).

delete_job(Job) when is_map(Job) ->
  tivan_server:remove(?MODULE, norn_jobs, Job);
delete_job(Name) ->
  tivan_server:remove(?MODULE, norn_jobs, #{name => Name}).

put_log(Log) when is_map(Log) ->
  tivan_server:put(?MODULE, norn_logs, Log).

get_logs() ->
  get_logs(#{}).

get_logs(Options) when is_map(Options) ->
  tivan_server:get(?MODULE, norn_logs, Options).

update_logs(Options, Updates) when is_map(Options), is_map(Updates) ->
  tivan_server:update(?MODULE, norn_logs, Options, Updates).

delete_log(Log) when is_map(Log) ->
  tivan_server:remove(?MODULE, norn_logs, Log);
delete_log(LogUuid) ->
  tivan_server:remove(?MODULE, norn_logs, #{uuid => LogUuid}).

%%%===================================================================
%%% tivan_server callbacks
%%%===================================================================

init([]) ->
  TableDefs = #{
    norn_jobs => #{columns => #{name => #{type => binary
                                          ,key => true}
                                ,function => #{type => function
                                               ,null => false}
                                ,minutes => #{type => [integer]
                                              ,limit => {0, 59}}
                                ,hours => #{type => [integer]
                                            ,limit => {0, 24}}
                                ,days_month => #{type => [integer]
                                                 ,limit => {1, 31}}
                                ,days_week => #{type => [integer]
                                                ,limit => {1, 7}}
                                ,month => #{type => [integer]
                                            ,limit => {1, 12}}
                                ,timezone => #{type => [binary]}
                                ,expiry => #{type => second}
                                ,run_time => #{type => datetime}
                                ,status => #{type => binary
                                             ,limit => [<<"active">>
                                                        ,<<"inactive">>]
                                             ,default => <<"active">>}}
                   ,audit => true}
   ,norn_logs => #{columns => #{name => #{type => norns
                                         ,index => true
                                         ,null => false}
                               ,run_time => #{type => datetime
                                             ,index => true
                                             ,null => false}
                               ,run_pid => #{type => pid}
                               ,run_result => #{type => term}}}
   },
  {ok, TableDefs}.
