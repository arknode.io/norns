-module(norns).

-export([put_job/1
        ,get_jobs/0
        ,get_jobs/1
        ,update_jobs/2
        ,delete_job/1
        ,put_log/1
        ,get_logs/0
        ,get_logs/1
        ,update_logs/2
        ,delete_log/1]).

put_job(Norn) -> norns_tivan:put_job(Norn).

get_jobs() -> norns_tivan:get_jobs().

get_jobs(Options) -> norns_tivan:get_jobs(Options).

update_jobs(Options, Updates) -> norns_tivan:update_jobs(Options, Updates).

delete_job(Norn) -> norns_tivan:delete_job(Norn).

put_log(Log) -> norns_tivan:put_log(Log).

get_logs() -> norns_tivan:get_logs().

get_logs(Options) -> norns_tivan:get_logs(Options).

update_logs(Options, Updates) -> norns_tivan:update_logs(Options, Updates).

delete_log(Log) -> norns_tivan:delete(Log).
